﻿using System.Collections.Generic;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Infrastructure.Dictionaries
{
    public class DisplayError
    {
        public static Dictionary<StringKind, string> Errors { get; private set; }

        static DisplayError()
        {
            Errors = new Dictionary<StringKind, string>
            {
                {StringKind.FirstName,"FirstName must be from 2 to 60 characters long and contain only A-Za-z characters." },
                {StringKind.LastName,"LastName must be from 2 to 60 characters long and contain only A-Za-z characters." },
                {StringKind.Email,"Email format is incorrect. Valid email is 'example@domain'." },
                {StringKind.PhoneNumber,"PhoneNumber must be from 11 to 15 characters long, contain only numbers and must start with '+'." },
                {StringKind.Birthday,"Employee must be from 18 to 100 years old" },
                {StringKind.PositionTitle,"Title must be from 2 to 60 characters long and contain only A-Z a-z characters." }
            };
        }
    }
}
