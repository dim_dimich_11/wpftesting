﻿using System.Collections.Generic;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Infrastructure.Dictionaries
{
    public class Pattern
    {
        static Pattern()
        {
            Patterns = new Dictionary<StringKind, string>
            {
                {StringKind.FirstName,@"^[A-Za-z]{2,60}$" },
                {StringKind.LastName,@"^[A-Za-z]{2,60}$" },
                {StringKind.Email, @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}" },
                {StringKind.PhoneNumber,@"^(\+[0-9]{1,4})[0-9]{3}[0-9]{7,8}$" },
                {StringKind.PositionTitle,@"^[A-Z- a-z]{2,60}$" },
                {StringKind.CityName,@"^[A-Z- a-z]{2,60}" }
            };
        }

        public static Dictionary<StringKind, string> Patterns { get; private set; }
    }
}
