﻿namespace Ribbon.Infrastructure.DisplayModels
{
    public class LocalizationDisplayModel : BaseDisplayModel
    {
        private string _localization;

        private string _imagePath;

        public string Localization
        {
            get { return _localization; }
            set
            {
                _localization = value;
                OnPropertyChanged();
            }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set
            {
                _imagePath = value;
                OnPropertyChanged();
            }
        }
    }
}
