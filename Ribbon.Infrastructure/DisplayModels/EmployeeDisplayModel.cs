﻿using System;
using System.ComponentModel;
using Ribbon.Infrastructure.Attributes;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.DisplayModels
{
    public class EmployeeDisplayModel : BaseDisplayModel, ICloneable
    {
        private string _firstName;

        private string _lastName;

        private string _email;

        private string _phoneNumber;

        private DateTime _birthday;

        [ShouldBeValidated]
        [FieldForSorting]
        [Description("First name")]
        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }

        [ShouldBeValidated]
        [FieldForSorting]
        [Description("Last name")]
        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }

        [Description("Full name")]
        public string FullName { get; set; }

        [Description("Position")]
        public Position Position { get; set; }

        [ShouldBeValidated]
        [Description("Email")]
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                OnPropertyChanged();
            }
        }

        [ShouldBeValidated]
        [Description("Phone number")]
        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                _phoneNumber = value;
                OnPropertyChanged();
            }
        }

        public Guid CityId { get; set; }

        [Description("City")]
        public City City { get; set; }

        [ShouldBeValidated]
        [Description("Birthday")]
        public DateTime Birthday
        {
            get { return _birthday; }
            set
            {
                _birthday = value;
                OnPropertyChanged();
            }
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
