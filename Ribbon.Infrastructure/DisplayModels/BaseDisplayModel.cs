﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using Ribbon.Infrastructure.Attributes;
using Ribbon.Infrastructure.Extensions;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.DisplayModels
{
    public class BaseDisplayModel : BaseEntity, IDataErrorInfo, INotifyPropertyChanged
    {
        public bool EnableValidate { get; set; }

        public string this[string columnName]
        {
            get { return Validate(columnName); }
        }

        public string Error
        {
            get { return CheckProperties(); }
        }

        private string Validate(string propertyName)
        {
            var property = GetType().GetProperty(propertyName);
            string result = string.Empty;

            if (property != null)
            {
                if (property.PropertyType == typeof(string))
                {
                    result = ValidateStringProperty(property);
                }
                else if (property.PropertyType == typeof(DateTimeOffset))
                {
                    result = ValidateDateTimeOffsetProperty(property);
                }
            }

            return result;
        }

        public void RaiseAllProperties()
        {
            IEnumerable<PropertyInfo> properties = GetType().GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(ShouldBeValidatedAttribute)));

            foreach (var property in properties)
            {
                OnPropertyChanged(property.Name);
            }
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        protected string CheckProperties()
        {
            if (!EnableValidate)
            {
                return null;
            }

            IDataErrorInfo currentModel = this;
            var properties = GetType().GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(ShouldBeValidatedAttribute))).Select(p => p.Name);

            var result = string.Empty;

            foreach (var propertyName in properties)
            {
                var propertyError = currentModel[propertyName];

                if (!string.IsNullOrWhiteSpace(propertyError))
                {
                    result = propertyError;
                    break;
                }
            }

            return result;
        }

        private string ValidateStringProperty(PropertyInfo property)
        {
            string error = string.Empty;
            var regex = new Regex(property.GetPropertyPattern());

            if (EnableValidate)
            {
                if (property.GetValue(this) != null)
                {
                    var propertyValue = property.GetValue(this).ToString();

                    if (!regex.IsMatch(propertyValue))
                    {
                        error = property.GetPropertyError();
                    }
                }
                else
                {
                    error = string.Format("{0} is required.", property.Name);
                }
            }

            return error;
        }

        private string ValidateDateTimeOffsetProperty(PropertyInfo property)
        {
            var propertyValue = (DateTimeOffset)property.GetValue(this);
            var maxDate = DateTimeOffset.UtcNow.AddYears(-100);
            var minDate = DateTimeOffset.UtcNow.AddYears(-18);

            return propertyValue < maxDate || propertyValue > minDate
                ? property.GetPropertyError()
                : string.Empty;
        }
    }
}
