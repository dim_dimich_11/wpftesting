﻿using Ribbon.Infrastructure.Attributes;

namespace Ribbon.Infrastructure.DisplayModels
{
    public class CityDisplayModel : BaseDisplayModel
    {
        private string _cityName;

        [ShouldBeValidated]
        public string CityName
        {
            get { return _cityName; }
            set
            {
                _cityName = value;
                OnPropertyChanged();
            }
        }
    }
}
