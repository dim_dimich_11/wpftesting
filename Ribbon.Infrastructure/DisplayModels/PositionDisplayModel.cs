﻿using Ribbon.Infrastructure.Attributes;

namespace Ribbon.Infrastructure.DisplayModels
{
    public class PositionDisplayModel : BaseDisplayModel
    {
        private string _title;

        [ShouldBeValidated]
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }
    }
}
