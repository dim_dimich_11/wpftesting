﻿using System;
using System.Reflection;
using Ribbon.Infrastructure.Dictionaries;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Infrastructure.Extensions
{
    public static class PropertyInfoExtension
    {
        public static string GetPropertyError(this PropertyInfo property)
        {
            string error = string.Empty;

            var propertyOwner = property.DeclaringType;
            StringKind stringKind;

            if (propertyOwner != null && Enum.TryParse(property.Name, out stringKind))
            {
                error = DisplayError.Errors[stringKind];
            }

            return error;
        }

        public static string GetPropertyPattern(this PropertyInfo property)
        {
            string pattern = string.Empty;

            Type propertyOwner = property.DeclaringType;
            StringKind stringKind;

            if (propertyOwner != null && Enum.TryParse(property.Name, out stringKind))
            {
                pattern = Pattern.Patterns[stringKind];
            }

            return pattern;
        }
    }
}
