﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.Extensions
{
    public static class GenericExtension
    {
        public static string PropertyName<T, TEntity>(this T obj, Expression<Func<T, TEntity>> propertyExpression)
        {
            if (propertyExpression.Body.NodeType != ExpressionType.MemberAccess)
            {
                return null;
            }

            if (!(propertyExpression.Body is MemberExpression))
            {
                return null;
            }

            return ((MemberExpression)propertyExpression.Body).Member.Name;
        }

        public static int GetElementIndex<T>(this List<T> entities, Guid positionId) where T : BaseEntity
        {
            int positionIndex = entities.Select((entity, index) => new { entity, index })
                .First(p => p.entity.Id == positionId).index;

            return positionIndex;
        }
    }
}
