﻿using System.Data.Entity;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Infrastructure
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Position> Positions { get; set; }
    }
}
