﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Ribbon.Infrastructure.Infrastructure
{
    public class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
    }
}
