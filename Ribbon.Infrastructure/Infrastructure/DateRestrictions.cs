﻿using System;

namespace Ribbon.Infrastructure.Infrastructure
{
    public class DateRestrictions
    {
        public DateTime MinDate { get; set; }

        public DateTime MaxDate { get; set; }

        public DateRestrictions()
        {
            MinDate = DateTime.UtcNow.AddYears(-100);
            MaxDate = DateTime.UtcNow.AddYears(-18);
        }
    }
}
