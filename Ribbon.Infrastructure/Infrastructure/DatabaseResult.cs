﻿using System;

namespace Ribbon.Infrastructure.Infrastructure
{
    public class DatabaseResult
    {
        public bool Success { get; set; }

        public string Message { get; set; }

        public object Entity { get; set; }

        public Exception Exception { get; set; }
    }
}