﻿using Ribbon.Infrastructure.Enums;

namespace Ribbon.Infrastructure.Infrastructure
{
    public class WindowResult
    {
        public UserAction Action { get; set; }

        public object Entity { get; set; }
    }
}
