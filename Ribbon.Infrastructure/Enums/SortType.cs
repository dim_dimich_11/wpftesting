﻿namespace Ribbon.Infrastructure.Enums
{
    public enum SortType
    {
        Ascending,
        Descending
    }
}
