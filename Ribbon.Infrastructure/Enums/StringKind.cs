﻿using System;
using System.ComponentModel;

namespace Ribbon.Infrastructure.Enums
{
    [Flags]
    public enum StringKind
    {
        None,
        [Description("First Name")]
        FirstName,
        LastName,
        CityName,
        PositionTitle,
        PhoneNumber,
        Email,
        Birthday
    }
}
