﻿namespace Ribbon.Infrastructure.Enums
{
    public enum UserAction
    {
        None,
        Edit,
        Add,
        Delete
    }
}
