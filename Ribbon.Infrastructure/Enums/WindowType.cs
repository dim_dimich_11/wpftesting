﻿namespace Ribbon.Infrastructure.Enums
{
    public enum WindowType
    {
        AppWindow,
        ModalWindow
    }
}
