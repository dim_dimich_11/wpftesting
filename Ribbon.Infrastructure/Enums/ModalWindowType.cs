﻿namespace Ribbon.Infrastructure.Enums
{
    public enum ModalWindowType
    {
        Info,
        Error,
        Success
    }
}
