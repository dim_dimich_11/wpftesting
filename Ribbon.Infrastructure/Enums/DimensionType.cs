﻿namespace Ribbon.Infrastructure.Enums
{
    public enum DimensionType
    {
        Width,
        Height
    }
}
