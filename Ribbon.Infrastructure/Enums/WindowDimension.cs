﻿namespace Ribbon.Infrastructure.Enums
{
    public enum WindowDimension
    {
        AppWindowWidth = 800,
        AppWindowHeight = 450,
        ModalWindowWidth = 300
    }
}
