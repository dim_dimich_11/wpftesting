namespace Ribbon.Infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class changeEmployeeModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "CityId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Employees", "CityId");
            AddForeignKey("dbo.Employees", "CityId", "dbo.Cities", "Id", cascadeDelete: true);
            DropColumn("dbo.Employees", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Employees", "Location", c => c.String());
            DropForeignKey("dbo.Employees", "CityId", "dbo.Cities");
            DropIndex("dbo.Employees", new[] { "CityId" });
            DropColumn("dbo.Employees", "CityId");
        }
    }
}
