﻿using System;

namespace Ribbon.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldForSorting : Attribute
    {
    }
}
