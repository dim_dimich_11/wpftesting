﻿using System;

namespace Ribbon.Infrastructure.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ShouldBeValidatedAttribute : Attribute
    {
    }
}
