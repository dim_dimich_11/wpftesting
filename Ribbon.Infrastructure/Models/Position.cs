﻿using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.Models
{
    public class Position : BaseEntity
    {
        public string Title { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}
