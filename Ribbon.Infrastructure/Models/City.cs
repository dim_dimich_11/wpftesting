﻿using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.Models
{
    public class City : BaseEntity
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
