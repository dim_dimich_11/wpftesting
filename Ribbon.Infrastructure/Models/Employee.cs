﻿using System;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.Models
{
    public class Employee : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public Guid PositionId { get; set; }

        public virtual Position Position { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public Guid CityId { get; set; }

        public virtual City City { get; set; }

        public DateTimeOffset Birthday { get; set; }
    }
}
