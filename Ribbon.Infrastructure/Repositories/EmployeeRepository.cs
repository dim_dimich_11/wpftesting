﻿using System;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Repositories
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext context) : base(context)
        {
        }

        public async Task<DatabaseResult> GetAllEmployeesAsync()
        {
            return await FindManyAsync(x => true);
        }

        public async Task<DatabaseResult> GetEmployeesAsync(Guid cityId)
        {
            return await FindManyAsync(e => e.CityId == cityId);
        }

        public async Task<DatabaseResult> GetEmployeesAsync(Expression<Func<Employee, bool>> predicate,
            Expression<Func<Employee, object>> sortExpression, SortType sortType)
        {
            return await FindManyAsync(predicate, sortExpression, sortType);
        }

        public async Task<DatabaseResult> AddEmployeeAsync(Employee employee)
        {
            return await InsertOneAsync(employee);
        }

        public async Task<DatabaseResult> EditEmployeeAsync(Employee employee)
        {
            return await UpdateOneAsync(e => e.Id == employee.Id, employee);
        }

        public async Task<DatabaseResult> DeleteEmployeeAsync(Guid employeeId)
        {
            return await DeleteOneAsync(e => e.Id == employeeId);
        }
    }
}
