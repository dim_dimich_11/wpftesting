﻿using System.Data.Entity;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Repositories
{
    public class CityRepository : GenericRepository<City>, ICityRepository
    {
        public CityRepository(DbContext context) : base(context)
        {
        }

        public async Task<DatabaseResult> GetAllCitiesAsync()
        {
            return await FindManyAsync(x => true);
        }

        public async Task<DatabaseResult> AddCityAsync(City city)
        {
            return await InsertOneAsync(city);
        }
    }
}
