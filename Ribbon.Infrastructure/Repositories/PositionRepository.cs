﻿using System.Data.Entity;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Repositories
{
    public class PositionRepository : GenericRepository<Position>, IPositionRepository
    {
        public PositionRepository(DbContext context) : base(context)
        {
        }

        public async Task<DatabaseResult> AddPositionAsync(Position position)
        {
            return await InsertOneAsync(position);
        }

        public async Task<DatabaseResult> GetAllPositionsAsync()
        {
            return await FindManyAsync(x => true);
        }
    }
}
