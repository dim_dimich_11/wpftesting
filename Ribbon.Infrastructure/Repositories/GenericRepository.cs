﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;

namespace Ribbon.Infrastructure.Repositories
{
    public abstract class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {

        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        protected GenericRepository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public async Task<DatabaseResult> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = int.MaxValue, int skip = 0)
        {
            var dbResult = new DatabaseResult();

            try
            {
                dbResult.Entity = await _dbSet.Where(predicate).OrderBy(predicate).Skip(skip).Take(limit).ToListAsync();
                dbResult.Success = true;
            }
            catch (Exception e)
            {
                dbResult.Message = e.Message;
                dbResult.Exception = e;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> FindManyAsync(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> sortExpression, SortType sortType)
        {
            var dbResult = new DatabaseResult();

            try
            {
                dbResult.Entity = sortType == SortType.Ascending
                    ? await _dbSet.Where(predicate).OrderBy(sortExpression).ToListAsync()
                    : await _dbSet.Where(predicate).OrderByDescending(sortExpression).ToListAsync();

                dbResult.Success = true;
            }
            catch (Exception e)
            {
                dbResult.Message = e.Message;
                dbResult.Exception = e;
            }

            return dbResult;
        }

        public async Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.FirstOrDefaultAsync(predicate);
        }

        public async Task<DatabaseResult> InsertOneAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();

            try
            {
                _dbSet.Add(entity);

                await _context.SaveChangesAsync();
                ((IObjectContextAdapter)_context).ObjectContext.Detach(entity);
                dbResult.Success = true;
                dbResult.Entity = _dbSet.FirstOrDefault(e => e.Id == entity.Id);
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> InsertManyAsync(List<TEntity> entityList)
        {
            var dbResult = new DatabaseResult();

            try
            {
                _dbSet.AddRange(entityList);

                await _context.SaveChangesAsync();

                dbResult.Success = true;
                dbResult.Entity = entityList;
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> UpdateOneAsync(TEntity entity)
        {
            var dbResult = new DatabaseResult();

            try
            {
                _context.Entry(entity).State = EntityState.Modified;

                await _context.SaveChangesAsync();

                dbResult.Success = true;
                dbResult.Entity = entity;
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> UpdateOneAsync(Expression<Func<TEntity, bool>> predicate, TEntity entity)
        {
            var dbResult = new DatabaseResult();

            try
            {
                var dbEntity = await _dbSet.SingleOrDefaultAsync(predicate);

                if (dbEntity != null)
                {
                    _context.Entry(dbEntity).CurrentValues.SetValues(entity);
                    await _context.SaveChangesAsync();
                }

                dbResult.Success = true;
                dbResult.Entity = dbEntity;
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> UpdateManyAsync(List<TEntity> entityList)
        {
            var dbResult = new DatabaseResult();

            try
            {
                foreach (var entity in entityList)
                {
                    _context.Entry(entity).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }

                dbResult.Success = true;
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> DeleteOneAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var dbResult = new DatabaseResult();

            try
            {
                var result = await _dbSet.FirstOrDefaultAsync(predicate);

                if (result != null)
                {
                    _dbSet.Remove(result);
                    await _context.SaveChangesAsync();

                    dbResult.Success = true;
                }
                else
                {
                    dbResult.Message = "Entity not found.";
                }
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }

        public async Task<DatabaseResult> DeleteManyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var dbResult = new DatabaseResult();

            try
            {
                var result = _dbSet.Where(predicate).ToList();
                if (result.Count != 0)
                {
                    foreach (var entity in result)
                    {
                        _dbSet.Remove(entity);
                    }

                    await _context.SaveChangesAsync();
                    dbResult.Success = true;
                }
                else
                {
                    dbResult.Message = "Error Nothing was found by expression.";
                }
            }
            catch (Exception exp)
            {
                dbResult.Message = exp.Message;
                dbResult.Exception = exp;
            }

            return dbResult;
        }
    }
}
