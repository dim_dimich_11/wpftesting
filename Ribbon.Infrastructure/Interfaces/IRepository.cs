﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.Infrastructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        #region Find

        Task<DatabaseResult> FindManyAsync(Expression<Func<TEntity, bool>> predicate, int limit = 20, int skip = 0);
        
        Task<DatabaseResult> FindManyAsync(Expression<Func<TEntity, bool>> predicate,
            Expression<Func<TEntity, object>> sorExpression, SortType sortType);

        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> predicate);
        #endregion

        #region Insert
        Task<DatabaseResult> InsertOneAsync(TEntity entity);

        Task<DatabaseResult> InsertManyAsync(List<TEntity> entityList);
        #endregion

        #region Update

        Task<DatabaseResult> UpdateOneAsync(TEntity entity);

        Task<DatabaseResult> UpdateOneAsync(Expression<Func<TEntity, bool>> predicate, TEntity entity);

        Task<DatabaseResult> UpdateManyAsync(List<TEntity> entityList);
        #endregion

        #region Delete

        Task<DatabaseResult> DeleteOneAsync(Expression<Func<TEntity, bool>> predicate);

        Task<DatabaseResult> DeleteManyAsync(Expression<Func<TEntity, bool>> predicate);
        #endregion
    }
}
