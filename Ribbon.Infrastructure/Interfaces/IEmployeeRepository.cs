﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Interfaces
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<DatabaseResult> GetAllEmployeesAsync();
        Task<DatabaseResult> GetEmployeesAsync(Guid cityId);

        Task<DatabaseResult> GetEmployeesAsync(Expression<Func<Employee, bool>> predicate,
            Expression<Func<Employee, object>> sortExpression, SortType sortType);

        Task<DatabaseResult> AddEmployeeAsync(Employee employee);

        Task<DatabaseResult> EditEmployeeAsync(Employee employee);

        Task<DatabaseResult> DeleteEmployeeAsync(Guid employeeId);
    }
}
