﻿using System.Threading.Tasks;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Interfaces
{
    public interface ICityRepository : IRepository<City>
    {
        Task<DatabaseResult> GetAllCitiesAsync();
        Task<DatabaseResult> AddCityAsync(City city);
    }
}
