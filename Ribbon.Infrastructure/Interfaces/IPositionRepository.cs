﻿using System.Threading.Tasks;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Models;

namespace Ribbon.Infrastructure.Interfaces
{
    public interface IPositionRepository : IRepository<Position>
    {
        Task<DatabaseResult> AddPositionAsync(Position position);
        Task<DatabaseResult> GetAllPositionsAsync();
    }
}
