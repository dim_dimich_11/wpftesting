﻿using System;
using System.Globalization;
using System.Windows.Data;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Converters
{
    public class WindowTypeToWindowSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = (int)WindowDimension.AppWindowWidth;

            if (value != null)
            {
                var windowType = (WindowType)value;
                var dimensionType = (DimensionType?)parameter ?? DimensionType.Width;

                switch (windowType)
                {
                    case WindowType.AppWindow:
                        result = dimensionType == DimensionType.Width
                            ? (int)WindowDimension.AppWindowWidth
                            : (int)WindowDimension.AppWindowHeight;

                        break;
                    case WindowType.ModalWindow:
                        result = dimensionType == DimensionType.Width
                            ? (int)WindowDimension.ModalWindowWidth
                            : 0;
                        break;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
