﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Converters
{
    public class WindowTypeToResizeModeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = ResizeMode.CanResize;

            if (value is WindowType)
            {
                var windowType = (WindowType)value;
                result = windowType == WindowType.AppWindow ? ResizeMode.CanResize : ResizeMode.NoResize;
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
