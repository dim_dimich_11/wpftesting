﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Ribbon.Converters
{
    public class CountToVisibilityConverter : IValueConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">Count of elements</param>
        /// <param name="targetType"></param>
        /// <param name="parameter">Show element if count more than zero</param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = Visibility.Collapsed;

            if (value is int && parameter != null)
            {
                var typedValue = (int)value;
                var typedParameter = bool.Parse((string)parameter);

                if (typedValue > 0 && typedParameter)
                {
                    result = Visibility.Visible;
                }
                else if (typedValue == 0 && !typedParameter)
                {
                    result = Visibility.Visible;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
