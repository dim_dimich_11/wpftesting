﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Data;

namespace Ribbon.Converters
{
    public class PropertyDescriptionToNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = string.Empty;

            if (value != null && parameter != null)
            {
                var propertiesDictionary = (Dictionary<string, string>)parameter;

                propertiesDictionary.TryGetValue(value.ToString(), out result);
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
