﻿using System;
using System.Globalization;
using System.Windows.Data;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Converters
{
    public class ModalWindowTypeToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = "";

            if (value is ModalWindowType)
            {
                var modalWindowType = (ModalWindowType)value;
                result = string.Format("pack://application:,,,/Images/modal-{0}.png", modalWindowType.ToString().ToLower());
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
