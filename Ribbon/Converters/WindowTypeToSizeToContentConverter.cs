﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Converters
{
    public class WindowTypeToSizeToContentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = SizeToContent.Manual;

            if (value == null)
            {
                return result;
            }

            var windowType = (WindowType)value;
            result = windowType == WindowType.AppWindow ? result : SizeToContent.Height;

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
