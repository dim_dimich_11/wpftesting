﻿using System.Windows;

namespace Ribbon.Controls
{
    /// <summary>
    /// Interaction logic for Loader.xaml
    /// </summary>
    public partial class Loader
    {

        public static readonly DependencyProperty IsTurnedOnProperty = DependencyProperty.Register(
            "IsTurnedOn", typeof(bool), typeof(Loader));

        public bool IsTurnedOn
        {
            get { return (bool) GetValue(IsTurnedOnProperty); }
            set { SetValue(IsTurnedOnProperty, value); }
        }

        public Loader()
        {
            InitializeComponent();
        }
    }
}
