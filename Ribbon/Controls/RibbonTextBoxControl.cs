﻿using System.Windows;
using System.Windows.Controls;

namespace Ribbon.Controls
{
    public class RibbonTextBoxControl : TextBox
    {
        public static readonly DependencyProperty HintProperty = DependencyProperty.Register(
            "Hint", typeof(string), typeof(RibbonTextBoxControl));

        public string Hint
        {
            get { return (string)GetValue(HintProperty); }
            set { SetValue(HintProperty, value); }
        }
    }
}
