﻿using System.Data.Entity;
using Prism.Events;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Repositories;
using Unity;
using Unity.Injection;

namespace Ribbon.Factories
{
    public static class UnityFactory
    {
        private static IUnityContainer _container;

        /// <summary>
        /// Use to initialize unity container
        /// </summary>
        /// <returns>Returns container</returns>
        public static IUnityContainer Initialize()
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            _container = container;

            return _container;
        }

        /// <summary>
        /// Register different types to container
        /// </summary>
        /// <param name="container">Instance of container to register</param>
        public static void RegisterTypes(IUnityContainer container)
        {

            //context
            container.RegisterType<DbContext, ApplicationDbContext>();

            //event aggregator
            container.RegisterType<IEventAggregator, EventAggregator>();

            //Repositories
            container.RegisterType<IEmployeeRepository, EmployeeRepository>(
                new InjectionConstructor(container.Resolve<DbContext>()));

            container.RegisterType<ICityRepository, CityRepository>(
                new InjectionConstructor(container.Resolve<DbContext>()));

            container.RegisterType<IPositionRepository, PositionRepository>(
                new InjectionConstructor(container.Resolve<DbContext>()));
        }

        /// <summary>
        /// Resolves object from container
        /// </summary>
        /// <typeparam name="T">Type to resolve from container</typeparam>
        /// <param name="name">Name of instance used to resolve</param>
        /// <returns>Instance of resolved type</returns>
        public static T ResolveObject<T>(string name = null)
        {
            return _container.Resolve<T>(name);
        }
    }
}
