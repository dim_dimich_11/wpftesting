﻿using System.Windows;
using Ribbon.EventArgs;
using Ribbon.Infrastructure.Enums;
using Ribbon.ViewModels;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for DeleteEmployeeView.xaml
    /// </summary>
    public partial class DeleteEmployeeView
    {
        public DeleteEmployeeView()
        {
            InitializeComponent();
        }

        protected override void OnCloseWindow(object sender, WindowArgs e)
        {
            var window = Window.GetWindow(this);

            if (window != null)
            {
                var vm = (DeleteEmployeeViewModel)sender;
                window.DialogResult = vm.DeleteWindowResult.Action == UserAction.Delete;
                window.Close();
            }
        }
    }
}
