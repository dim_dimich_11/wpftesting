﻿using Ribbon.ViewModels;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for CreatePositionView.xaml
    /// </summary>
    public partial class CreateNewPositionView
    {
        public CreateNewPositionView()
        {
            InitializeComponent();
        }

        protected override void OnVmAttached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as CreateNewPositionViewModel;

            if (vm != null)
            {
                vm.CloseWindow += OnCloseWindow;
            }
        }

        protected override void OnVmDetached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as CreateNewPositionViewModel;

            if (vm != null)
            {
                vm.CloseWindow -= OnCloseWindow;
            }
        }
    }
}
