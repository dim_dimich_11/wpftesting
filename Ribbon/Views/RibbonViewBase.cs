﻿using System.Reflection;
using System.Resources;
using System.Windows;
using System.Windows.Controls;
using Mapster;
using Prism.Events;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Parameters;
using Ribbon.ViewModels;
using Ribbon.Views.Modals;

namespace Ribbon.Views
{
    public class RibbonViewBase : UserControl
    {
        #region Declarations
        private RibbonBaseViewModel _viewModel;

        public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register(
            "Header", typeof(string), typeof(RibbonViewBase), new PropertyMetadata(default(string)));

        protected static readonly ResourceManager ResourceManager = new ResourceManager(
            string.Format("{0}.Properties.Resources", Assembly.GetExecutingAssembly().GetName().Name),
            Assembly.GetExecutingAssembly());

        protected static readonly IEventAggregator EventAggregator;
        #endregion

        #region Properties
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }
        #endregion

        #region Constructors

        static RibbonViewBase()
        {
            EventAggregator = UnityFactory.ResolveObject<IEventAggregator>();
        }

        public RibbonViewBase()
        {
            DataContextChanged += RibbonViewBaseDataContextChanged;
            Loaded += RibbonViewBaseLoaded;
            Unloaded += RibbonViewBaseUnloaded;
        }
        #endregion

        #region Methods
        protected virtual void OnVmDetached(RibbonBaseViewModel viewModel)
        {

        }

        protected virtual void OnVmAttached(RibbonBaseViewModel viewModel)
        {

        }

        protected virtual void OnCloseWindow(object sender, WindowArgs args)
        {
            var window = Window.GetWindow(this);

            if (window != null)
            {
                if (args.InfoViewSettings != null)
                {
                    ShowInfoView(args.InfoViewSettings);
                }

                window.DialogResult = true;
                window.Close();
            }
        }

        protected bool ShowModalWindow(ShowModalParams parameters)
        {
            var modalWindow = new ModalWindowBase(parameters.View, parameters.ModalName);
            var modalResult = modalWindow.ShowDialog();

            if (modalResult.HasValue && modalResult.Value && parameters.Action != null)
            {
                parameters.Action();
                return true;
            }

            return false;
        }

        private void RibbonViewBaseLoaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.OnFirstLoadExecuted();
                _viewModel.ShowErrorMessage += OnShowErrorMessage;
                _viewModel.CloseWindow += OnCloseWindow;
            }
        }

        private void RibbonViewBaseUnloaded(object sender, RoutedEventArgs e)
        {
            if (_viewModel != null)
            {
                _viewModel.OnUnloadExecuted();
                _viewModel.ShowErrorMessage -= OnShowErrorMessage;
                _viewModel.CloseWindow -= OnCloseWindow;
            }
        }
        private void OnShowErrorMessage(object sender, WindowArgs args)
        {
            ShowInfoView(args.InfoViewSettings);
        }

        private void ShowInfoView(InfoViewSettings settings)
        {
            var modalWindowParameters = new ShowModalParams
            {
                View = settings.Adapt<InfoView>(),
                ModalName = settings.ModalWindowTitle
            };

            ShowModalWindow(modalWindowParameters);
        }

        private void RibbonViewBaseDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = e.NewValue as RibbonBaseViewModel;

            if (viewModel != null)
            {
                _viewModel = viewModel;
                OnVmAttached(_viewModel);
            }
            else
            {
                OnVmDetached(_viewModel);
            }
        }
        #endregion
    }
}