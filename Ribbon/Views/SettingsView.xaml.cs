﻿using System.Windows;
using Ribbon.EventArgs;
using Ribbon.Events;
using Ribbon.Parameters;
using Ribbon.ViewModels;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        protected override void OnVmAttached(RibbonBaseViewModel viewModel)
        {
            ((SettingsViewModel)viewModel).SavePressed += OnCloseSettingsWindow;
            EventAggregator.GetEvent<InfoModalAccept>().Subscribe(RestartApplication);
        }

        protected override void OnVmDetached(RibbonBaseViewModel viewModel)
        {
            ((SettingsViewModel)viewModel).SavePressed -= OnCloseSettingsWindow;
            EventAggregator.GetEvent<InfoModalAccept>().Unsubscribe(RestartApplication);
        }

        private void OnCloseSettingsWindow(object sender, SettingsEventArgs args)
        {
            if (args.LocalizationChanged)
            {
                var modalWindowParameters = new ShowModalParams
                {
                    View = new InfoView
                    {
                        Text = ResourceManager.GetString("InfoModalText"),
                        AcceptButtonContent = ResourceManager.GetString("YesButton"),
                        RejectButtonContent = ResourceManager.GetString("NoButton")
                    },
                    ModalName = ResourceManager.GetString("InfoModalName")
                };

                ShowModalWindow(modalWindowParameters);
            }

            OnCloseWindow(sender, new WindowArgs());
        }

        private void RestartApplication()
        {
            Application.Current.Shutdown();
            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);
        }
    }
}
