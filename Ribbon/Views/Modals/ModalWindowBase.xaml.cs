﻿using System.Windows;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Views.Modals
{
    /// <summary>
    /// Interaction logic for ModalWindowBase.xaml
    /// </summary>
    public partial class ModalWindowBase : Window
    {
        public static readonly DependencyProperty ViewProperty = DependencyProperty.Register(
            "View", typeof(RibbonViewBase), typeof(ModalWindowBase));

        public static readonly DependencyProperty WindowTypeProperty = DependencyProperty.Register(
            "WindowType", typeof(WindowType), typeof(ModalWindowBase), new PropertyMetadata(WindowType.ModalWindow));

        public WindowType WindowType
        {
            get { return (WindowType)GetValue(WindowTypeProperty); }
            set { SetValue(WindowTypeProperty, value); }
        }

        public RibbonViewBase View
        {
            get { return (RibbonViewBase)GetValue(ViewProperty); }
            private set { SetValue(ViewProperty, value); }
        }

        public ModalWindowBase(RibbonViewBase viewBase,string modalTitle)
        {
            InitializeComponent();
            View = viewBase;
            Title = modalTitle;
        }
    }
}
