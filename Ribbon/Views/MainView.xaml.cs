﻿using Mapster;
using Ribbon.EventArgs;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;
using Ribbon.Util;
using Ribbon.ViewModels;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for BaseView.xaml
    /// </summary>
    public partial class MainView
    {
        public MainView()
        {
            InitializeComponent();
        }

        protected override void OnVmAttached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as MainViewModel;

            if (vm != null)
            {
                vm.CreateNewPosition += OnCreateNewPosition;
                vm.OpenSettings += OnOpenSettings;
                vm.CreateNewCity += OnCreateNewCity;
            }
        }

        protected override void OnVmDetached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as MainViewModel;

            if (vm != null)
            {
                vm.CreateNewPosition -= OnCreateNewPosition;
                vm.OpenSettings -= OnOpenSettings;
                vm.CreateNewCity -= OnCreateNewCity;
            }
        }

        private void OnCreateNewPosition(object sender, CreatePositionEventArgs args)
        {
            var createNewPositionView = new CreateNewPositionView();
            var modalWindowParameters = new ShowModalParams
            {
                View = createNewPositionView,
                ModalName = ResourceManager.GetString("NewPositionModalName"),
                Action = () =>
                {
                    args.Position = ((CreateNewPositionViewModel)createNewPositionView.DataContext).Position;
                }
            };

            ShowModalWindow(modalWindowParameters);
        }

        private void OnOpenSettings(object sender, System.EventArgs args)
        {
            var modalWindowParameters = new ShowModalParams
            {
                View = new SettingsView(),
                ModalName = ResourceManager.GetString("SettingsModalName")
            };

            bool modalResult = ShowModalWindow(modalWindowParameters);

            if (!modalResult)
            {
                var currentTheme = RibbonHelper.GetCurrentTheme();

                if (currentTheme != null)
                {
                    currentTheme.Source = RibbonHelper.GetThemeUri(Properties.Settings.Default.CurrentThemeName);
                }
            }
        }

        private void OnCreateNewCity(object sender, CreateCityEventArgs args)
        {
            var cityView = new CreateCityView();
            var modalWindowParameters = new ShowModalParams
            {
                View = cityView,
                ModalName = ResourceManager.GetString("CreateCityModalName"),
                Action = () =>
                {
                    args.City = ((CreateCityViewModel)cityView.DataContext).City.Adapt<City>();
                }
            };

            ShowModalWindow(modalWindowParameters);
        }
    }
}
