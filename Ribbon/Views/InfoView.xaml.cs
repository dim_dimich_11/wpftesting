﻿using System.Windows;
using Ribbon.EventArgs;
using Ribbon.Events;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for InfoView.xaml
    /// </summary>
    public partial class InfoView
    {
        #region Properties
        public static readonly DependencyProperty AcceptButtonVisibilityProperty = DependencyProperty.Register(
            "AcceptButtonVisibility", typeof(Visibility), typeof(InfoView));

        public static readonly DependencyProperty RejectButtonVisibilityProperty = DependencyProperty.Register(
            "RejectButtonVisibility", typeof(Visibility), typeof(InfoView));

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof(string), typeof(InfoView));

        public static readonly DependencyProperty AcceptButtonContentProperty = DependencyProperty.Register(
            "AcceptButtonContent", typeof(string), typeof(InfoView));

        public static readonly DependencyProperty RejectButtonContentProperty = DependencyProperty.Register(
            "RejectButtonContent", typeof(string), typeof(InfoView));

        public static readonly DependencyProperty ModalWindowTypeProperty = DependencyProperty.Register(
            "ModalWindowType", typeof(ModalWindowType), typeof(InfoView), new PropertyMetadata(ModalWindowType.Info));

        public ModalWindowType ModalWindowType
        {
            get { return (ModalWindowType)GetValue(ModalWindowTypeProperty); }
            set { SetValue(ModalWindowTypeProperty, value); }
        }

        public Visibility AcceptButtonVisibility
        {
            get { return (Visibility)GetValue(AcceptButtonVisibilityProperty); }
            set { SetValue(AcceptButtonVisibilityProperty, value); }
        }

        public Visibility RejectButtonVisibility
        {
            get { return (Visibility)GetValue(RejectButtonVisibilityProperty); }
            set { SetValue(RejectButtonVisibilityProperty, value); }
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string AcceptButtonContent
        {
            get { return (string)GetValue(AcceptButtonContentProperty); }
            set { SetValue(AcceptButtonContentProperty, value); }
        }

        public string RejectButtonContent
        {
            get { return (string)GetValue(RejectButtonContentProperty); }
            set { SetValue(RejectButtonContentProperty, value); }
        }
        #endregion

        #region Constructors
        public InfoView()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods
        private void AcceptButtonClick(object sender, RoutedEventArgs e)
        {
            EventAggregator.GetEvent<InfoModalAccept>().Publish();
            OnCloseWindow(this, new WindowArgs());
        }

        private void DeclineButtonClick(object sender, RoutedEventArgs e)
        {
            EventAggregator.GetEvent<InfoModalDecline>().Publish();
            OnCloseWindow(this, new WindowArgs());
        }
        #endregion
    }
}
