﻿using System.Windows;
using Ribbon.EventArgs;
using Ribbon.Infrastructure.Enums;
using Ribbon.Parameters;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for CreateCityView.xaml
    /// </summary>
    public partial class CreateCityView
    {
        public CreateCityView()
        {
            InitializeComponent();
        }

        //protected override void OnVmAttached(RibbonBaseViewModel viewModel)
        //{
        //    ((CreateCityViewModel)viewModel).SavePressed += OnCloseWindow;
        //}

        //protected override void OnVmDetached(RibbonBaseViewModel viewModel)
        //{
        //    ((CreateCityViewModel)viewModel).SavePressed -= OnCloseWindow;
        //}

        protected override void OnCloseWindow(object sender, WindowArgs args)
        {
            var window = Window.GetWindow(this);

            if (window != null)
            {
                var modalWindowParameters = new ShowModalParams
                {
                    View = new InfoView
                    {
                        Text = ResourceManager.GetString("CityCreatedText"),
                        RejectButtonVisibility = Visibility.Collapsed,
                        ModalWindowType = ModalWindowType.Success,
                        AcceptButtonContent = ResourceManager.GetString("OkButton")
                    },
                    ModalName = ResourceManager.GetString("CreateCityModalName")
                };

                ShowModalWindow(modalWindowParameters);

                window.DialogResult = true;
                window.Close();
            }
        }
    }
}
