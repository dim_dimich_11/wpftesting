﻿using System;
using Mapster;
using Ribbon.EventArgs;
using Ribbon.Extensions;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Parameters;
using Ribbon.ViewModels;

namespace Ribbon.Views
{
    /// <summary>
    /// Interaction logic for CityView.xaml
    /// </summary>
    public partial class CityView
    {
        public CityView()
        {
            InitializeComponent();
        }

        protected override void OnVmAttached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as CityViewModel;

            if (vm != null)
            {
                vm.CreateOrUpdateEmployee += OnCreateOrUpdateEmployee;
                vm.DeleteEmployee += OnDeleteEmployee;
                vm.DuplicateEmployee += OnDuplicateCommand;
                vm.ExportToExcel += OnExportToExcel;
            }
        }

        protected override void OnVmDetached(RibbonBaseViewModel viewModel)
        {
            var vm = viewModel as CityViewModel;

            if (vm != null)
            {
                vm.CreateOrUpdateEmployee -= OnCreateOrUpdateEmployee;
                vm.DeleteEmployee -= OnDeleteEmployee;
                vm.DuplicateEmployee -= OnDuplicateCommand;
                vm.ExportToExcel -= OnExportToExcel;
            }
        }

        private void OnCreateOrUpdateEmployee(object sender, CreateOrUpdateEmployeeEventArgs e)
        {
            var createOrUpdateEmployeeView = new CreateOrUpdateEmployeeView
            {
                DataContext = new CreateOrUpdateEmployeeViewModel(e.EmployeeDisplayModel, e.Positions)
            };

            var modalWindowParameters = new ShowModalParams
            {
                View = createOrUpdateEmployeeView,
                ModalName = e.EmployeeDisplayModel.Id != Guid.Empty
                    ? ResourceManager.GetString("EditEmployeeModalName")
                    : ResourceManager.GetString("CreateEmployeeModalName"),
                Action = () =>
                {
                    e.Result = ((CreateOrUpdateEmployeeViewModel)createOrUpdateEmployeeView.DataContext).WindowResult;
                }
            };

            ShowModalWindow(modalWindowParameters);
        }

        private void OnDeleteEmployee(object sender, DeleteEmployeeEventArgs e)
        {
            var deleteEmployeeView = new DeleteEmployeeView
            {
                DataContext = new DeleteEmployeeViewModel(e.EmployeeId)
            };

            var modalWindowParameters = new ShowModalParams
            {
                View = deleteEmployeeView,
                ModalName = ResourceManager.GetString("DeleteEmployeeModalName"),
                Action = () =>
                {
                    e.WindowResult.Action = UserAction.Delete;
                }
            };

            ShowModalWindow(modalWindowParameters);
        }

        private void OnDuplicateCommand(object sender, DuplicateEventArgs args)
        {
            var duplicateView = new DuplicateView
            {
                DataContext = new DuplicateViewModel((EmployeeDisplayModel)args.Employee.Clone())
            };

            var modalWindowParameters = new ShowModalParams
            {
                View = duplicateView,
                ModalName = ResourceManager.GetString("CreateEmployeeModalName"),
                Action = () =>
                {
                    args.WindowResult = new WindowResult
                    {
                        Action = UserAction.Add,
                        Entity = (EmployeeDisplayModel)((DuplicateViewModel)duplicateView.DataContext).WindowResult.Entity
                    };
                }
            };

            ShowModalWindow(modalWindowParameters);
        }

        private void OnExportToExcel(object sender, ExportExcelEventArgs args)
        {
            CityDataGrid.ExportToExcel(args.FileName, args.FileExtension);

            var modalWindowParameters = new ShowModalParams
            {
                View = args.InfoViewSettings.Adapt<InfoView>(),
                ModalName = args.InfoViewSettings.ModalWindowTitle
            };

            ShowModalWindow(modalWindowParameters);
        }
    }

}
