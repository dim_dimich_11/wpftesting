﻿using System;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.EventArgs
{
    public class DeleteEmployeeEventArgs : System.EventArgs
    {
        public Guid EmployeeId { get; set; }

        public WindowResult WindowResult { get; set; }

        public DeleteEmployeeEventArgs(Guid employeeId)
        {
            EmployeeId = employeeId;
            WindowResult = new WindowResult();
        }
    }
}
