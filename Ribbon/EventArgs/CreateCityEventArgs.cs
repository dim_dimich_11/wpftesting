﻿using Ribbon.Infrastructure.Models;

namespace Ribbon.EventArgs
{
    public class CreateCityEventArgs : System.EventArgs
    {
        public City City { get; set; }
    }
}
