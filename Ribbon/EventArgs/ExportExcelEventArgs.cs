﻿using Ribbon.Parameters;

namespace Ribbon.EventArgs
{
    public class ExportExcelEventArgs : System.EventArgs
    {
        public string FileName { get; set; }

        public string FileExtension { get; set; }

        public InfoViewSettings InfoViewSettings { get; set; }
    }
}
