﻿using Ribbon.Parameters;

namespace Ribbon.EventArgs
{
    public class WindowArgs : System.EventArgs
    {
        public InfoViewSettings InfoViewSettings { get; set; }
    }
}
