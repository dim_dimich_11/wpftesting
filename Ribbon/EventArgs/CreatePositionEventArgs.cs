﻿using Ribbon.Infrastructure.DisplayModels;

namespace Ribbon.EventArgs
{
    public class CreatePositionEventArgs : System.EventArgs
    {
        public PositionDisplayModel Position { get; set; }
    }
}
