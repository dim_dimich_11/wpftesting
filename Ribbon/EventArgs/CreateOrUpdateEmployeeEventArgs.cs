﻿using System.Collections.Generic;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.EventArgs
{
    public class CreateOrUpdateEmployeeEventArgs : System.EventArgs
    {
        public EmployeeDisplayModel EmployeeDisplayModel { get; set; }

        public WindowResult Result { get; set; }

        public List<PositionDisplayModel> Positions { get; set; }

        public CreateOrUpdateEmployeeEventArgs(EmployeeDisplayModel model, List<PositionDisplayModel> positions)
        {
            EmployeeDisplayModel = model;
            Positions = positions;
            Result = new WindowResult();
        }
    }

    public delegate void CreateOrUpdateEmployeeEventHandler(object sender, CreateOrUpdateEmployeeEventArgs args);
}
