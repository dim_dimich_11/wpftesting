﻿using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Infrastructure;

namespace Ribbon.EventArgs
{
    public class DuplicateEventArgs : System.EventArgs
    {
        public EmployeeDisplayModel Employee { get; set; }

        public WindowResult WindowResult { get; set; }
    }
}
