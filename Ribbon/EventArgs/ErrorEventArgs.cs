﻿using System;

namespace Ribbon.EventArgs
{
    public class ErrorEventArgs : System.EventArgs
    {
        public Exception ExceptionInfo { get; set; }
    }
}
