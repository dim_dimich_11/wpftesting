﻿namespace Ribbon.EventArgs
{
    public class SettingsEventArgs : System.EventArgs
    {
        public bool LocalizationChanged { get; set; }
    }
}
