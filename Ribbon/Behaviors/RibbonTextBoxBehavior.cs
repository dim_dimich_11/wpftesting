﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ribbon.Controls;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Behaviors
{
    public static class RibbonTextBoxBehavior
    {
        #region Declaration

        public static readonly DependencyProperty StringKindProperty = DependencyProperty.RegisterAttached(
            "StringKind", typeof(StringKind), typeof(RibbonTextBoxBehavior),
            new PropertyMetadata(StringKind.None, OnPropertyChanged));

        #endregion

        #region Methods

        public static StringKind GetStringKind(DependencyObject obj)
        {
            return (StringKind)obj.GetValue(StringKindProperty);
        }

        public static void SetStringKind(DependencyObject obj, StringKind value)
        {
            obj.SetValue(StringKindProperty, value);
        }

        private static void OnPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var control = sender as RibbonTextBoxControl;

            if (control != null)
            {
                control.Loaded -= ControlOnLoaded;
                control.Unloaded -= ControlOnUnloaded;

                control.Loaded += ControlOnLoaded;
                control.Unloaded += ControlOnUnloaded;
            }
        }

        private static void ControlOnLoaded(object sender, RoutedEventArgs e)
        {
            var control = (RibbonTextBoxControl)sender;

            control.TextChanged += ControlOnTextChanged;
            control.PreviewTextInput += Control_PreviewTextInput;
        }

        private static void ControlOnUnloaded(object sender, RoutedEventArgs e)
        {
            var control = (RibbonTextBoxControl)sender;

            control.PreviewTextInput -= Control_PreviewTextInput;
            control.TextChanged -= ControlOnTextChanged;
        }

        private static void Control_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var control = sender as RibbonTextBoxControl;

            if (control != null)
            {
                var regex = GetRegexByStringKind(control);

                var result = string.Format("{0}{1}", control.Text, e.Text);

                if (regex.IsMatch(result))
                {
                    if (GetStringKind(control) == StringKind.CityName ||
                        GetStringKind(control) == StringKind.PositionTitle)
                    {
                        result = Regex.Replace(result, @"[-]{2,}", "-");
                    }

                    control.Text = result;
                }

                control.CaretIndex = control.Text.Length;
                e.Handled = true;
            }
        }

        private static void ControlOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var control = sender as RibbonTextBoxControl;

            if (control != null)
            {
                control.Text = control.Text.Replace(" ", "");
                control.CaretIndex = control.Text.Length;
            }
        }

        private static Regex GetRegexByStringKind(RibbonTextBoxControl control)
        {
            Regex regex;

            switch (GetStringKind(control))
            {
                case StringKind.FirstName:
                    regex = new Regex(@"^[A-za-z]+$");
                    break;

                case StringKind.LastName:
                    regex = new Regex(@"^[A-za-z]+$");
                    break;

                case StringKind.CityName:
                    regex = new Regex(@"^[A-za-z]{0,}[A-z-a-z]{1,37}$");
                    break;

                case StringKind.PositionTitle:
                    regex = new Regex(@"^[A-za-z][A-z-a-z]{0,58}$");
                    break;

                case StringKind.PhoneNumber:
                    regex = new Regex(@"^(\+[0-9]{0,15}$)");
                    break;
                default:
                    regex = new Regex(@"^[A-za-z]+$");
                    break;
            }

            return regex;
        }

        #endregion
    }

}
