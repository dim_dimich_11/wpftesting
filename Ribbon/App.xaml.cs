﻿using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Windows;
using GemBox.Spreadsheet;
using Mapster;
using Ribbon.Factories;
using Ribbon.Infrastructure.Enums;
using Ribbon.Util;
using Ribbon.Views;
using Ribbon.Views.Modals;

namespace Ribbon
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            TypeAdapterConfig.GlobalSettings.Scan(Assembly.GetEntryAssembly());
            //Initialize unity
            UnityFactory.Initialize();
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            //Mapster Configuration
            MapsterConfiguration.Configure();

            var currentTheme = RibbonHelper.GetCurrentTheme();

            if (currentTheme != null)
            {
                currentTheme.Source = RibbonHelper.GetThemeUri(Ribbon.Properties.Settings.Default.CurrentThemeName);
            }

            Thread.CurrentThread.CurrentUICulture =
                new CultureInfo(Ribbon.Properties.Settings.Default.CurrentLocalization);

            var view = new ModalWindowBase(new MainView(), "Testing project")
            {
                WindowType = WindowType.AppWindow
            };

            view.ShowDialog();
        }
    }
}
