﻿using System;
using System.Linq;
using System.Windows;

namespace Ribbon.Util
{
    public static class RibbonHelper
    {
        public static ResourceDictionary GetCurrentTheme()
        {
            return Application.Current.Resources.MergedDictionaries.FirstOrDefault(x =>
                 x.Source.OriginalString.Contains("Theme"));
        }

        public static Uri GetThemeUri(string themeName)
        {
            return new Uri(
                string.Format("/{0};component/Resources/Themes/{1}Theme.xaml",
                    typeof(RibbonHelper).Assembly.GetName().Name, themeName), UriKind.RelativeOrAbsolute);
        }
    }
}
