﻿using System;
using System.Collections.Generic;
using Mapster;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;
using Ribbon.ViewModels;
using Ribbon.Views;

namespace Ribbon.Util
{
    public class MapsterConfiguration
    {
        public static void Configure()
        {
            TypeAdapterConfig<EmployeeDisplayModel, Employee>.NewConfig()
                .Map(dest => dest.Birthday, src => new DateTimeOffset(src.Birthday, TimeSpan.Zero))
                .Map(dest => dest.FullName, src => string.Format("{0} {1}", src.FirstName, src.LastName))
                .Map(dest => dest.Email, src => src.Email.ToLower())
                .Map(dest => dest.PositionId, src => (Guid)MapContext.Current.Parameters["positionId"])
                .Ignore(dest => dest.Position).Ignore(dest => dest.City);

            TypeAdapterConfig<Employee, EmployeeDisplayModel>.NewConfig()
                .Map(dest => dest.Birthday, src => src.Birthday.DateTime);

            TypeAdapterConfig<Position, PositionDisplayModel>.NewConfig();

            TypeAdapterConfig<City, CityView>.NewConfig().Map(dest => dest.Header, src => MapDataContextForCityViewModel(src))
                .Map(dest => dest.DataContext,
                    src => MapDataContextToCityViewModel(src,
                        (List<PositionDisplayModel>)MapContext.Current.Parameters["positions"]));

            TypeAdapterConfig<CityDisplayModel, City>.NewConfig().Map(dest => dest.Name, src => src.CityName);
            TypeAdapterConfig<City, CityDisplayModel>.NewConfig().Map(dest => dest.CityName, src => src.Name);
            TypeAdapterConfig<InfoViewSettings, InfoView>.NewConfig();
        }

        private static string MapDataContextForCityViewModel(City city)
        {
            return city != null ? city.Name : string.Empty;
        }

        private static CityViewModel MapDataContextToCityViewModel(City city, List<PositionDisplayModel> positions)
        {
            var result = new CityViewModel(city, positions);

            return result;
        }
    }
}
