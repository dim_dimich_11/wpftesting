﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Controls;
using GemBox.Spreadsheet;

namespace Ribbon.Extensions
{
    public static class DataGridExtension
    {
        public static void ExportToExcel(this DataGrid dataGrid, string fileName, string fileExtension)
        {
            var excelFile = new ExcelFile();

            List<object> items = dataGrid.ItemsSource.Cast<object>().ToList();
            List<PropertyInfo> itemProperties = items[0].GetType().GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(DescriptionAttribute))).ToList();

            ExcelWorksheet excelSheet = excelFile.Worksheets.Add("Sheet1");

            for (var i = 0; i < items.Count; i++)
            {
                if (i == 0)
                {
                    PrintHeader(excelSheet, itemProperties);
                }

                for (int j = 0; j < itemProperties.Count; j++)
                {
                    var item = items[i];
                    var currentPropertyValue = itemProperties[j].GetValue(item);

                    excelSheet.Cells[i + 1, j].Value = currentPropertyValue is DateTime
                        ? ((DateTime)currentPropertyValue).ToString("MM/dd/yyyy")
                        : currentPropertyValue.ToString();
                }
            }

            excelFile.Save(string.Format("{0}\\{1}.{2}",
                Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName, fileExtension));
        }

        private static void PrintHeader(ExcelWorksheet excelSheet, List<PropertyInfo> itemProperties)
        {
            for (int j = 0; j < itemProperties.Count; j++)
            {
                var currentProperty = itemProperties[j];

                excelSheet.Cells[0, j].Value =
                    ((DescriptionAttribute)currentProperty.GetCustomAttribute(typeof(DescriptionAttribute), false))
                    .Description;
            }
        }
    }
}
