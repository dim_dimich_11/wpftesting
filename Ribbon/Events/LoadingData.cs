﻿using Prism.Events;

namespace Ribbon.Events
{
    public class LoadingData : PubSubEvent<bool>
    {
    }
}
