﻿using System;
using Ribbon.Views;

namespace Ribbon.Parameters
{
    public class ShowModalParams
    {
        public string ModalName { get; set; }

        public RibbonViewBase View { get; set; }

        public Action Action { get; set; }
    }
}
