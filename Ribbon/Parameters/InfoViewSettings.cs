﻿using System.Windows;
using Ribbon.Infrastructure.Enums;

namespace Ribbon.Parameters
{
    public class InfoViewSettings
    {
        public string Text { get; set; }

        public Visibility AcceptButtonVisibility { get; set; }

        public Visibility RejectButtonVisibility { get; set; }

        public string AcceptButtonContent { get; set; }

        public string RejectButtonContent { get; set; }

        public ModalWindowType ModalWindowType { get; set; }

        public string ModalWindowTitle { get; set; }
    }
}
