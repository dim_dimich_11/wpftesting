﻿using Ribbon.Infrastructure.Enums;

namespace Ribbon.Parameters
{
    public class FilterEmployeesParameters
    {
        public SortType SortType { get; set; }

        public string SortingField { get; set; }

        public override string ToString()
        {
            return SortingField;
        }
    }
}
