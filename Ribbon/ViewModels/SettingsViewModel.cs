﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Util;

namespace Ribbon.ViewModels
{
    public class SettingsViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private List<string> _themes;
        private List<LocalizationDisplayModel> _localizations;
        private string _selectedTheme;
        private LocalizationDisplayModel _selectedLocalization;
        public event EventHandler<SettingsEventArgs> SavePressed;
        #endregion

        #region Propertiees
        public ApplicationCommand SaveCommand { get; set; }

        public List<string> Themes
        {
            get { return _themes; }
            set
            {
                _themes = value;
                OnPropertyChanged();
            }
        }

        public List<LocalizationDisplayModel> Localizations
        {
            get { return _localizations; }
            set
            {
                _localizations = value;
                OnPropertyChanged();
            }
        }

        public string SelectedTheme
        {
            get { return _selectedTheme; }
            set
            {
                _selectedTheme = value;
                OnPropertyChanged();
                ChangeTheme();
            }
        }

        public LocalizationDisplayModel SelectedLocalization
        {
            get { return _selectedLocalization; }
            set
            {
                _selectedLocalization = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors
        public SettingsViewModel()
        {
            SelectedTheme = Properties.Settings.Default.CurrentThemeName;
            SaveCommand = new ApplicationCommand(Save);
        }
        #endregion

        #region Methods

        protected override void Initialize()
        {
            Themes = new List<string>
            {
                "Green",
                "Yellow",
                "Blue"
            };

            Localizations = new List<LocalizationDisplayModel>
            {
                new LocalizationDisplayModel
                {
                    Localization = "en-US",
                    ImagePath = "pack://application:,,,/Images/usa.png"
                },
                new LocalizationDisplayModel
                {
                    Localization = "uk",
                    ImagePath = "pack://application:,,,/Images/ukraine.png"
                }
            };

            SelectedLocalization =
                Localizations.First(x => x.Localization == Properties.Settings.Default.CurrentLocalization);
        }

        private void Save(object obj)
        {
            var localizationChanged = Properties.Settings.Default.CurrentLocalization != _selectedLocalization.Localization;
            Properties.Settings.Default.CurrentThemeName = _selectedTheme;
            Properties.Settings.Default.CurrentLocalization = _selectedLocalization.Localization;
            Properties.Settings.Default.Save();

            OnSavePressed(localizationChanged);
        }

        private void ChangeTheme()
        {
            var currentTheme = RibbonHelper.GetCurrentTheme();

            if (currentTheme != null && !currentTheme.Source.OriginalString.Contains(_selectedTheme))
            {
                currentTheme.Source = RibbonHelper.GetThemeUri(_selectedTheme);
            }
        }
        #endregion

        #region Event invokators
        private void OnSavePressed(bool localizationChanged)
        {
            var handler = SavePressed;

            if (handler != null)
            {
                var args = new SettingsEventArgs
                {
                    LocalizationChanged = localizationChanged
                };

                handler(this, args);
            }
        }
        #endregion
    }
}
