﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ribbon.Infrastructure.Models;

namespace Ribbon.ViewModels
{
    public class WelcomeViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private ObservableCollection<City> _cities;

        #endregion

        #region Propertiees
        public ObservableCollection<City> Cities
        {
            get
            {
                return _cities;
            }
            set
            {
                _cities = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public WelcomeViewModel(List<City> cities)
        {
            Cities = new ObservableCollection<City>(cities);
        }

        #endregion
    }
}
