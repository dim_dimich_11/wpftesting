﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Prism.Commands;
using Prism.Events;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Events;
using Ribbon.Factories;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public abstract class RibbonBaseViewModel : INotifyPropertyChanged
    {
        #region Declarations
        private bool _isLoading;
        private int _selectedItemIndex;
        protected static readonly IEventAggregator EventAggregator;

        protected virtual List<Tuple<CompositeCommand, ICommand>> Commands { get; set; }
        protected static readonly ResourceManager ResourceManager = new ResourceManager(
            string.Format("{0}.Properties.Resources", Assembly.GetExecutingAssembly().GetName().Name),
            Assembly.GetExecutingAssembly());

        private ApplicationCommand _closeCommand;

        public event EventHandler<WindowArgs> CloseWindow;
        public event EventHandler<WindowArgs> ShowErrorMessage;
        #endregion

        #region Propertiees
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                OnPropertyChanged();
            }
        }

        public int SelectedItemIndex
        {
            get { return _selectedItemIndex; }
            set
            {
                _selectedItemIndex = value;
                OnPropertyChanged();
            }
        }

        public ApplicationCommand CloseCommand
        {
            get { return _closeCommand; }
            set
            {
                _closeCommand = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors
        static RibbonBaseViewModel()
        {
            EventAggregator = UnityFactory.ResolveObject<IEventAggregator>();
        }

        protected RibbonBaseViewModel()
        {
            _closeCommand = new ApplicationCommand(CloseWindowExecuted);
        }

        #endregion

        #region Methods
        public async void OnFirstLoadExecuted()
        {
            Initialize();
            SubscribeOnEvents();
            await RunTaskAsync(OnFirstLoadAsync());
        }

        public void OnUnloadExecuted()
        {
            UnsubscribeFromEvents();
        }

        protected bool CanCommandExecute(object parameter)
        {
            return !IsLoading;
        }

        protected virtual void SubscribeOnEvents()
        {
            if (Commands != null && Commands.Any())
            {
                foreach (var commandPair in Commands)
                {
                    var compositeCommand = commandPair.Item1;

                    if (compositeCommand != null)
                    {
                        compositeCommand.RegisterCommand(commandPair.Item2);
                    }
                }
            }
        }

        protected virtual void UnsubscribeFromEvents()
        {
            if (Commands != null && Commands.Any())
            {
                foreach (var commandPair in Commands)
                {
                    var compositeCommand = commandPair.Item1;

                    if (compositeCommand != null)
                    {
                        compositeCommand.UnregisterCommand(commandPair.Item2);
                    }
                }
            }
        }
        protected virtual void Initialize()
        {
        }

        protected Task RunTaskAsync(Task task)
        {
            return RunTaskAsync(async token =>
            {
                await task;
            });
        }

        protected Task<TResult> RunTaskAsync<TResult>(Task<TResult> task)
        {
            return RunTaskAsync(async token => await task);
        }

        protected Task RunTaskAsync(Func<CancellationToken, Task> task)
        {
            return RunTaskAsync(async token =>
            {
                await task(token);
                return (object)null;
            });
        }

        protected async Task<TResult> RunTaskAsync<TResult>(Func<CancellationToken, Task<TResult>> task)
        {
            TResult result;
            try
            {
                EventAggregator.GetEvent<LoadingData>().Publish(true);
                IsLoading = true;
                //await Task.Delay(10000);
                result = await task(CancellationToken.None);

                var databaseResult = result as DatabaseResult;

                if (databaseResult != null && !databaseResult.Success)
                {
                    var infoViewSettings = new InfoViewSettings
                    {
                        Text = databaseResult.Exception.Message,
                        ModalWindowType = ModalWindowType.Error,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("ErrorModalName")
                    };

                    OnShowErrorMessage(new WindowArgs
                    {
                        InfoViewSettings = infoViewSettings
                    });
                }
            }
            finally
            {
                IsLoading = false;
                EventAggregator.GetEvent<LoadingData>().Publish(false);
            }

            return result;
        }

        protected virtual Task OnFirstLoadAsync()
        {
            return Task.Delay(0);
        }
        #endregion

        #region Event invokators
        protected void OnCloseWindow(WindowArgs args)
        {
            var handler = CloseWindow;

            if (handler != null)
            {
                handler(this, args);
            }
        }

        protected void OnShowErrorMessage(WindowArgs args)
        {
            var handler = ShowErrorMessage;

            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void CloseWindowExecuted(object parameter = null)
        {
            OnCloseWindow(new WindowArgs());
        }

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            var handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(prop));
            }
        }

        #endregion
    }
}