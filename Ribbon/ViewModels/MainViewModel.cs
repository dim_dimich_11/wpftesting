﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Mapster;
using Prism.Commands;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Events;
using Ribbon.Factories;
using Ribbon.Infrastructure.Attributes;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;
using Ribbon.Views;

namespace Ribbon.ViewModels
{
    public class MainViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private readonly ICityRepository _cityRepository;
        private readonly IPositionRepository _positionRepository;
        private ApplicationCommand _createNewPositionCommand;
        private ApplicationCommand _openSettingsCommand;
        private ApplicationCommand _selectCityCommand;
        private ApplicationCommand _createNewCityCommand;
        private ApplicationCommand _clearFilterCommand;
        private List<Tuple<CompositeCommand, ICommand>> _commands;
        private ObservableCollection<RibbonViewBase> _views = new ObservableCollection<RibbonViewBase>();
        private Dictionary<string, string> _filterFields;
        private KeyValuePair<string, string> _filterField;
        private List<SortType> _sortTypes;
        private SortType _sortType;
        private FilterEmployeesParameters _filterEmployeesParameters;
        public event EventHandler<CreatePositionEventArgs> CreateNewPosition;
        public event EventHandler OpenSettings;
        public event EventHandler<CreateCityEventArgs> CreateNewCity;
        #endregion

        #region Propertiees

        public ObservableCollection<RibbonViewBase> Views
        {
            get { return _views; }
            set
            {
                _views = value;
                OnPropertyChanged();
            }
        }

        public List<PositionDisplayModel> Positions { get; private set; }

        public Dictionary<string, string> FilterFields
        {
            get { return _filterFields; }
            set
            {
                _filterFields = value;
                OnPropertyChanged();
            }
        }

        public KeyValuePair<string, string> FilterField
        {
            get { return _filterField; }
            set
            {
                _filterField = value;
                OnPropertyChanged();
                FilterEmployeesParameters.SortingField = _filterField.Value;
            }
        }

        public List<SortType> SortTypes
        {
            get { return _sortTypes; }
            set
            {
                _sortTypes = value;
                OnPropertyChanged();
            }
        }

        public SortType SortType
        {
            get { return _sortType; }
            set
            {
                _sortType = value;
                OnPropertyChanged();
                FilterEmployeesParameters.SortType = _sortType;
            }
        }

        public FilterEmployeesParameters FilterEmployeesParameters
        {
            get { return _filterEmployeesParameters; }
            set
            {
                _filterEmployeesParameters = value;
                OnPropertyChanged();
            }
        }

        protected override List<Tuple<CompositeCommand, ICommand>> Commands
        {
            get { return _commands; }
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            _cityRepository = UnityFactory.ResolveObject<ICityRepository>();
            _positionRepository = UnityFactory.ResolveObject<IPositionRepository>();
        }
        #endregion

        #region Methods

        protected override void SubscribeOnEvents()
        {
            EventAggregator.GetEvent<LoadingData>().Subscribe(OnLoadingData);
            base.SubscribeOnEvents();
        }

        protected override void UnsubscribeFromEvents()
        {
            EventAggregator.GetEvent<LoadingData>().Unsubscribe(OnLoadingData);
            base.UnsubscribeFromEvents();
        }

        protected sealed override void Initialize()
        {
            FilterFields = typeof(EmployeeDisplayModel).GetProperties()
                .Where(p => Attribute.IsDefined(p, typeof(FieldForSorting))).ToDictionary(
                    key => ((DescriptionAttribute)key.GetCustomAttribute(typeof(DescriptionAttribute), false))
                        .Description, value => value.Name);

            FilterEmployeesParameters = new FilterEmployeesParameters();

            SortTypes = Enum.GetValues(typeof(SortType)).Cast<SortType>().ToList();

            _createNewPositionCommand = new ApplicationCommand(CreateNewPositionExecute, CanCreateNewPositionExecute);
            _openSettingsCommand = new ApplicationCommand(OpenSettingsExecute, CanOpenSettingsExecute);
            _selectCityCommand = new ApplicationCommand(SelectCity, CanSelectCityExecute);
            _createNewCityCommand = new ApplicationCommand(CreateNewCityExecute, CanCreateNewCityExecute);
            _clearFilterCommand = new ApplicationCommand(ClearFilterExecute, CanCommandExecute);

            _commands = new List<Tuple<CompositeCommand, ICommand>>
            {
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.CreateNewPositionCommand,_createNewPositionCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.OpenSettingsCommand,_openSettingsCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.SelectCityCommand,_selectCityCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.CreateNewCityCommand,_createNewCityCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.ClearFilterCommand,_clearFilterCommand)
            };
        }

        protected override async Task OnFirstLoadAsync()
        {
            DatabaseResult getCitiesResult = await RunTaskAsync(_cityRepository.GetAllCitiesAsync());

            if (getCitiesResult.Success)
            {
                var cities = (List<City>)getCitiesResult.Entity;

                DatabaseResult getPositionsResult = await RunTaskAsync(_positionRepository.GetAllPositionsAsync());

                if (getPositionsResult.Success)
                {
                    Positions = ((List<Position>)getPositionsResult.Entity).Adapt<List<PositionDisplayModel>>();

                    //Properties.Settings.Default.Reset();
                    if (Properties.Settings.Default.ApplicationFirstLoad)
                    {
                        Views.Add(new WelcomeView
                        {
                            DataContext = new WelcomeViewModel(cities)
                        });

                        Properties.Settings.Default.ApplicationFirstLoad = false;
                        Properties.Settings.Default.Save();
                    }
                    else
                    {
                        //todo move it to mapster
                        foreach (var city in cities)
                        {
                            var cityView = new CityView
                            {
                                Header = city.Name,
                                DataContext = new CityViewModel(city, Positions)
                            };

                            Views.Add(cityView);
                        }
                    }
                }
            }
        }

        private bool CanCreateNewPositionExecute(object arg)
        {
            return !IsLoading;
        }

        private void CreateNewPositionExecute(object parameter)
        {
            var handler = CreateNewPosition;
            var args = new CreatePositionEventArgs();

            if (handler != null)
            {
                handler(this, args);

                if (args.Position != null)
                {
                    Positions.Add(args.Position);
                }
            }
        }

        private void OpenSettingsExecute(object parameter)
        {
            var handler = OpenSettings;

            if (handler != null)
            {
                handler(this, System.EventArgs.Empty);
            }
        }

        private bool CanOpenSettingsExecute(object parameter)
        {
            return !IsLoading;
        }

        private void SelectCity(object parameter)
        {
            var city = (City)parameter;

            var cityViews = Views.Where(v => v.DataContext.GetType() == typeof(CityViewModel)).ToList();

            if (cityViews.Count == 0 || cityViews.All(x => ((CityViewModel)x.DataContext).City.Id != city.Id))
            {
                AddCityView(city);
            }
            else
            {
                SelectedItemIndex = Views.Select((cityView, index) => new { cityView, index })
                    .First(v => v.cityView.DataContext.GetType() == typeof(CityViewModel) &&
                                ((CityViewModel)v.cityView.DataContext).City.Id == city.Id).index;
            }
        }

        private bool CanSelectCityExecute(object parameter)
        {
            return !IsLoading;
        }

        private void CreateNewCityExecute(object parameter)
        {
            var handler = CreateNewCity;
            var args = new CreateCityEventArgs();

            if (handler != null)
            {
                handler(this, args);

                if (args.City != null)
                {
                    AddCityView(args.City);
                }
            }
        }

        private bool CanCreateNewCityExecute(object parameter)
        {
            return !IsLoading;
        }

        private void ClearFilterExecute(object parameter)
        {
            FilterEmployeesParameters = new FilterEmployeesParameters();
            FilterField = default(KeyValuePair<string, string>);
        }

        private void AddCityView(City city)
        {
            Views.Add(new CityView
            {
                Header = city.Name,
                DataContext = new CityViewModel(city, Positions)
            });

            SelectedItemIndex = Views.Count - 1;
        }

        #endregion

        #region Event invokators
        private void OnLoadingData(bool isLoading)
        {
            IsLoading = isLoading;
        }
        #endregion
    }
}