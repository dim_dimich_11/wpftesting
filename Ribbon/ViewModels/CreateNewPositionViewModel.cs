﻿using System.Threading.Tasks;
using System.Windows;
using Mapster;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class CreateNewPositionViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private readonly IPositionRepository _positionRepository;
        private PositionDisplayModel _position;

        #endregion

        #region Properties
        public AsyncApplicationCommand SaveCommand { get; set; }

        public PositionDisplayModel Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public CreateNewPositionViewModel()
        {
            _positionRepository = UnityFactory.ResolveObject<IPositionRepository>();
            Position = new PositionDisplayModel();
            SaveCommand = new AsyncApplicationCommand(SaveAsync);
        }

        #endregion

        #region Methods
        private async Task SaveAsync(object parameter)
        {
            Position.EnableValidate = true;
            Position.RaiseAllProperties();

            if (!string.IsNullOrWhiteSpace(Position.Error))
            {
                Position.EnableValidate = false;
                return;
            }

            DatabaseResult result =
                await RunTaskAsync(_positionRepository.AddPositionAsync(_position.Adapt<Position>()));

            if (result.Success)
            {
                Position = result.Entity.Adapt<PositionDisplayModel>();
                var args = new WindowArgs
                {
                    InfoViewSettings = new InfoViewSettings
                    {
                        Text = ResourceManager.GetString("PositionAddedText"),
                        ModalWindowType = ModalWindowType.Success,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("InfoModalName")
                    }
                };

                OnCloseWindow(args);
            }
        }
        #endregion
    }
}
