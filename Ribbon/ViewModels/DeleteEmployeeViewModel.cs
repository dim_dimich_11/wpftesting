﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class DeleteEmployeeViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private readonly IEmployeeRepository _employeeRepository;
        private readonly Guid _employeeId;
        private AsyncApplicationCommand _deleteEmployeeCommand;

        #endregion

        #region Propertiees
        public WindowResult DeleteWindowResult { get; set; }

        public AsyncApplicationCommand DeleteEmployeeCommand
        {
            get
            {
                return _deleteEmployeeCommand;
            }
            set
            {
                _deleteEmployeeCommand = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors
        public DeleteEmployeeViewModel(Guid employeeId)
        {
            _employeeId = employeeId;
            _employeeRepository = UnityFactory.ResolveObject<IEmployeeRepository>();
            _deleteEmployeeCommand = new AsyncApplicationCommand(DeleteEmployeeAsync);

            DeleteWindowResult = new WindowResult();
        }
        #endregion

        #region Methods
        private async Task DeleteEmployeeAsync(object arg)
        {
            var args = new WindowArgs();

            var result = await RunTaskAsync
                (_employeeRepository.DeleteEmployeeAsync(_employeeId));

            if (result.Success)
            {
                DeleteWindowResult.Action = UserAction.Delete;

                args.InfoViewSettings = new InfoViewSettings
                {
                    Text = ResourceManager.GetString("EmployeeDeletedText"),
                    ModalWindowType = ModalWindowType.Success,
                    RejectButtonVisibility = Visibility.Collapsed,
                    AcceptButtonContent = ResourceManager.GetString("OkButton"),
                    ModalWindowTitle = ResourceManager.GetString("InfoModalName")
                };
            }

            OnCloseWindow(args);
        }
        #endregion
    }
}
