﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Mapster;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class CreateOrUpdateEmployeeViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private EmployeeDisplayModel _employee;
        private PositionDisplayModel _selectedPosition;
        private Func<Employee, Task<DatabaseResult>> _saveTask;
        #endregion

        #region Propertiees
        public AsyncApplicationCommand SaveCommand { get; private set; }

        public WindowResult WindowResult { get; set; }

        public List<PositionDisplayModel> Positions { get; set; }

        public EmployeeDisplayModel Employee
        {
            get
            {
                return _employee;
            }
            set
            {
                _employee = value;
                OnPropertyChanged();
            }
        }

        public PositionDisplayModel SelectedPosition
        {
            get { return _selectedPosition; }
            set
            {
                _selectedPosition = value;
                OnPropertyChanged();
            }
        }

        public DateRestrictions DateRestrictions { get; set; }
        #endregion

        #region Constructors
        public CreateOrUpdateEmployeeViewModel(EmployeeDisplayModel employee, List<PositionDisplayModel> positions)
        {
            Positions = positions;
            Employee = employee;
            SaveCommand = new AsyncApplicationCommand(SaveAsync);
        }
        #endregion

        protected sealed override void Initialize()
        {
            DateRestrictions = new DateRestrictions();

            SelectedPosition = Employee.Id != Guid.Empty
                ? Positions.FirstOrDefault(x => x.Id == Employee.Position.Id)
                : Positions[0];

            Employee.Birthday = Employee.Birthday == DateTime.MinValue
                ? DateRestrictions.MaxDate
                : Employee.Birthday;

            var employeeRepository = UnityFactory.ResolveObject<IEmployeeRepository>();

            if (Employee.Id != Guid.Empty)
            {
                _saveTask = employeeRepository.EditEmployeeAsync;
                WindowResult = new WindowResult
                {
                    Action = UserAction.Edit
                };
            }
            else
            {
                _saveTask = employeeRepository.AddEmployeeAsync;
                WindowResult = new WindowResult
                {
                    Action = UserAction.Add
                };
            }
        }

        #region Methods
        private async Task SaveAsync(object parameter)
        {
            Employee.EnableValidate = true;
            Employee.RaiseAllProperties();

            if (!string.IsNullOrWhiteSpace(Employee.Error))
            {
                Employee.EnableValidate = false;
                return;
            }

            var employee = _employee.BuildAdapter().AddParameters("positionId", _selectedPosition.Id)
                .AdaptToType<Employee>();

            DatabaseResult databaseResult = await RunTaskAsync(_saveTask(employee));

            if (databaseResult.Success)
            {
                WindowResult.Entity = databaseResult.Entity.Adapt<EmployeeDisplayModel>();
                var args = new WindowArgs
                {
                    InfoViewSettings = new InfoViewSettings
                    {
                        Text = ResourceManager.GetString(WindowResult.Action == UserAction.Add ? "EmployeeCreatedText" : "EmployeeEditedText"),
                        ModalWindowType = ModalWindowType.Success,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("InfoModalName")
                    }
                };
                OnCloseWindow(args);
            }
        }

        #endregion
    }
}
