﻿using System.Threading.Tasks;
using System.Windows;
using Mapster;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class CreateCityViewModel : RibbonBaseViewModel
    {
        #region Declaration
        private CityDisplayModel _city;

        private readonly ICityRepository _cityRepository;

        public AsyncApplicationCommand SaveCommand { get; set; }

        public CityDisplayModel City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Constructors

        public CreateCityViewModel()
        {
            SaveCommand = new AsyncApplicationCommand(SaveAsync);
            _cityRepository = UnityFactory.ResolveObject<ICityRepository>();
            City = new CityDisplayModel();
        }

        #endregion

        #region Methods

        private async Task SaveAsync(object parameter)
        {
            City.EnableValidate = true;
            City.RaiseAllProperties();

            if (!string.IsNullOrWhiteSpace(City.Error))
            {
                City.EnableValidate = false;
                return;
            }

            var adaptedCity = City.Adapt<City>();

            DatabaseResult insertResult = await RunTaskAsync(_cityRepository.AddCityAsync(adaptedCity));

            if (insertResult.Success)
            {
                City = insertResult.Entity.Adapt<CityDisplayModel>();

                var args = new WindowArgs
                {
                    InfoViewSettings = new InfoViewSettings
                    {
                        Text = ResourceManager.GetString("CityCreatedText"),
                        ModalWindowType = ModalWindowType.Success,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("InfoModalName")
                    }
                };

                OnCloseWindow(args);
            }

        }
        #endregion
    }
}
