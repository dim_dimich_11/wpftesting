﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Mapster;
using Prism.Commands;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Extensions;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class CityViewModel : RibbonBaseViewModel
    {
        #region Declaration
        private readonly IEmployeeRepository _employeeRepository;
        private City _city;
        private ObservableCollection<EmployeeDisplayModel> _employees;
        private readonly List<PositionDisplayModel> _positions;
        private List<Tuple<CompositeCommand, ICommand>> _commands;
        private ApplicationCommand _updateEmployeeCommand;
        private ApplicationCommand _createEmployeeCommand;
        private ApplicationCommand _deleteEmployeeCommand;
        private ApplicationCommand _duplicateCommand;
        private ApplicationCommand _exportToExcelCommand;
        private ApplicationCommand _exportToCsvCommand;
        private AsyncApplicationCommand _sortEmployeeCommand;
        private AsyncApplicationCommand _refreshCityDataCommand;
        public event CreateOrUpdateEmployeeEventHandler CreateOrUpdateEmployee;
        public event EventHandler<DeleteEmployeeEventArgs> DeleteEmployee;
        public event EventHandler<DuplicateEventArgs> DuplicateEmployee;
        public event EventHandler<ExportExcelEventArgs> ExportToExcel;
        #endregion

        #region Properties
        protected override List<Tuple<CompositeCommand, ICommand>> Commands
        {
            get { return _commands; }
        }

        public City City
        {
            get
            {
                return _city;
            }
            set
            {
                _city = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<EmployeeDisplayModel> Employees
        {
            get
            {
                return _employees;
            }
            set
            {
                _employees = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public CityViewModel(City city, List<PositionDisplayModel> positions)
        {
            City = city;
            SelectedItemIndex = -1;
            _positions = positions;
            _employeeRepository = UnityFactory.ResolveObject<IEmployeeRepository>();
        }
        #endregion

        #region Methods
        protected override async Task OnFirstLoadAsync()
        {
            DatabaseResult getEmployeesResult = await RunTaskAsync(_employeeRepository.GetEmployeesAsync(_city.Id));

            if (getEmployeesResult.Success)
            {
                Employees = new ObservableCollection<EmployeeDisplayModel>(((List<Employee>)getEmployeesResult.Entity)
                    .Adapt<List<EmployeeDisplayModel>>());
            }
        }

        protected override void Initialize()
        {
            _refreshCityDataCommand = new AsyncApplicationCommand(RefreshCityDataExecute, CanCommandExecute);
            _updateEmployeeCommand =
                new ApplicationCommand(CreateOrUpdateEmployeeExecute, IsItemSelected);
            _createEmployeeCommand = new ApplicationCommand(CreateOrUpdateEmployeeExecute, CanCommandExecute);
            _deleteEmployeeCommand = new ApplicationCommand(DeleteEmployeeExecute, IsItemSelected);
            _duplicateCommand = new ApplicationCommand(DuplicateEmployeeExecute, IsItemSelected);
            _sortEmployeeCommand = new AsyncApplicationCommand(SortEmployeesCommand, CanSortEmployeeCommandExecute);
            _exportToExcelCommand = new ApplicationCommand(ExportExecute, CanExportExecute);
            _exportToCsvCommand = new ApplicationCommand(ExportExecute, CanExportExecute);

            _commands = new List<Tuple<CompositeCommand, ICommand>>
            {
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.RefreshCityDataCommand,_refreshCityDataCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.UpdateEmployeeCommand,_updateEmployeeCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.CreateEmployeeCommand,_createEmployeeCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.DeleteEmployeeCommand, _deleteEmployeeCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.DuplicateEmployeeCommand,_duplicateCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.SortEmployeesCommand,_sortEmployeeCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.ExportToExcelCommand,_exportToExcelCommand),
                new Tuple<CompositeCommand, ICommand>(AppCompositeCommands.ExportToCsvCommand,_exportToCsvCommand)
            };
        }

        private bool IsItemSelected(object arg)
        {
            return !IsLoading && SelectedItemIndex != -1;
        }

        private void CreateOrUpdateEmployeeExecute(object parameter)
        {
            EmployeeDisplayModel employeeDisplay = (string)parameter == false.ToString()
                ? Employees[SelectedItemIndex].Adapt<EmployeeDisplayModel>()
                : new EmployeeDisplayModel
                {
                    CityId = City.Id
                };

            var args = new CreateOrUpdateEmployeeEventArgs(employeeDisplay, _positions);

            var handler = CreateOrUpdateEmployee;

            if (handler != null)
            {
                handler(this, args);
            }

            employeeDisplay = (EmployeeDisplayModel)args.Result.Entity;

            if (args.Result.Action == UserAction.Edit)
            {
                Employees[SelectedItemIndex] = employeeDisplay;
            }
            else if (args.Result.Action == UserAction.Add)
            {
                Employees.Add(employeeDisplay);
            }
        }

        private void DeleteEmployeeExecute(object parameter)
        {
            var eventArgs = new DeleteEmployeeEventArgs(_employees[SelectedItemIndex].Id);
            var handler = DeleteEmployee;

            if (handler != null)
            {
                handler(this, eventArgs);
            }

            if (eventArgs.WindowResult.Action == UserAction.Delete)
            {
                Employees.RemoveAt(SelectedItemIndex);
            }
        }

        private void DuplicateEmployeeExecute(object parameter)
        {
            var handler = DuplicateEmployee;

            if (handler != null)
            {
                var args = new DuplicateEventArgs
                {
                    WindowResult = new WindowResult(),
                    Employee = Employees[SelectedItemIndex].Clone() as EmployeeDisplayModel
                };

                handler(this, args);

                if (args.WindowResult.Action == UserAction.Add)
                {
                    Employees.Add((EmployeeDisplayModel)args.WindowResult.Entity);
                }
            }
        }

        private void ExportExecute(object parameter)
        {
            var handler = ExportToExcel;

            if (handler != null)
            {
                var fileName = string.Format("{0}Employees", City.Name);
                var fileExtension = parameter.ToString();
                var exportToExcelArgs = new ExportExcelEventArgs
                {
                    FileName = fileName,
                    FileExtension = fileExtension,
                    InfoViewSettings = new InfoViewSettings
                    {
                        Text = string.Format("{0} {1}\\{2}.{3}", ResourceManager.GetString("ExportText"),
                            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), fileName, fileExtension),
                        ModalWindowType = ModalWindowType.Success,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("ExportModalName")
                    }
                };

                handler(this, exportToExcelArgs);
            }
        }

        private bool CanExportExecute(object parameter)
        {
            return CanCommandExecute(parameter) && Employees.Count > 0;
        }

        private bool CanSortEmployeeCommandExecute(object parameter)
        {
            var filterParameter = parameter as FilterEmployeesParameters;

            return filterParameter != null && !string.IsNullOrEmpty(filterParameter.ToString());
        }

        private async Task SortEmployeesCommand(object parameter)
        {
            var filterParameter = parameter as FilterEmployeesParameters;
            if (filterParameter != null)
            {
                var getSortedEmployeesResult = new DatabaseResult();
                var sortedProperty = typeof(EmployeeDisplayModel).GetProperty(filterParameter.SortingField);

                if (sortedProperty != null && Employees.Count > 0)
                {
                    if (sortedProperty.Name == Employees[0].PropertyName(x => x.FirstName))
                    {
                        getSortedEmployeesResult =
                            await _employeeRepository.GetEmployeesAsync(x => true, e => e.FirstName,
                                filterParameter.SortType);
                    }
                    else if (sortedProperty.Name == Employees[0].PropertyName(x => x.LastName))
                    {
                        getSortedEmployeesResult =
                            await _employeeRepository.GetEmployeesAsync(x => true, e => e.LastName,
                                filterParameter.SortType);
                    }

                    var adaptedEmployees = getSortedEmployeesResult.Entity.Adapt<List<EmployeeDisplayModel>>();
                    Employees.Clear();
                    Employees.AddRange(adaptedEmployees);
                }
            }
        }

        private async Task RefreshCityDataExecute(object obj)
        {
            DatabaseResult getEmployeesResult = await RunTaskAsync(_employeeRepository.GetEmployeesAsync(City.Id));

            if (getEmployeesResult.Success)
            {
                Employees = new ObservableCollection<EmployeeDisplayModel>(((List<Employee>)getEmployeesResult.Entity)
                    .Adapt<List<EmployeeDisplayModel>>());

                SelectedItemIndex = -1;
            }
        }
        #endregion
    }
}
