﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Mapster;
using Ribbon.Commands;
using Ribbon.EventArgs;
using Ribbon.Factories;
using Ribbon.Infrastructure.DisplayModels;
using Ribbon.Infrastructure.Enums;
using Ribbon.Infrastructure.Infrastructure;
using Ribbon.Infrastructure.Interfaces;
using Ribbon.Infrastructure.Models;
using Ribbon.Parameters;

namespace Ribbon.ViewModels
{
    public class DuplicateViewModel : RibbonBaseViewModel
    {
        #region Declarations
        private EmployeeDisplayModel _duplicatedEmployee = new EmployeeDisplayModel();
        private IEmployeeRepository _employeeRepository;
        private bool _employeeChanged;
        #endregion

        #region Properties
        public EmployeeDisplayModel DuplicatedEmployee
        {
            get { return _duplicatedEmployee; }
            set
            {
                _duplicatedEmployee = value;
                OnPropertyChanged();
            }
        }

        public bool EmployeeChanged
        {
            get { return _employeeChanged; }
            set
            {
                _employeeChanged = value;
                OnPropertyChanged();
            }
        }

        private EmployeeDisplayModel OldEmployee { get; set; }

        public AsyncApplicationCommand SaveCommand { get; set; }

        public ApplicationCommand DuplicateEmployeeChanged { get; set; }

        public WindowResult WindowResult { get; set; }

        #endregion 

        #region Constructors
        public DuplicateViewModel(EmployeeDisplayModel duplicatedEmployee)
        {
            DuplicatedEmployee = duplicatedEmployee;
            OldEmployee = (EmployeeDisplayModel)duplicatedEmployee.Clone();
            SaveCommand = new AsyncApplicationCommand(SaveDuplicatedEmployeeAsync);
            DuplicateEmployeeChanged = new ApplicationCommand(DuplicateEmployeeChangedExecute);
        }

        protected sealed override void Initialize()
        {
            _employeeRepository = UnityFactory.ResolveObject<IEmployeeRepository>();
            WindowResult = new WindowResult();
        }

        #endregion

        #region Methods

        private void DuplicateEmployeeChangedExecute(object parameter)
        {
            EmployeeChanged = DuplicatedEmployee.FirstName != OldEmployee.FirstName ||
                              DuplicatedEmployee.LastName != OldEmployee.LastName;
        }

        private async Task SaveDuplicatedEmployeeAsync(object parameter)
        {
            DuplicatedEmployee.EnableValidate = true;
            DuplicatedEmployee.RaiseAllProperties();

            if (!string.IsNullOrWhiteSpace(DuplicatedEmployee.Error))
            {
                DuplicatedEmployee.EnableValidate = false;
                return;
            }

            var employee = _duplicatedEmployee.BuildAdapter().AddParameters("positionId", _duplicatedEmployee.Position.Id)
                .AdaptToType<Employee>();

            employee.Id = Guid.Empty;

            var insertResult = await RunTaskAsync(_employeeRepository.AddEmployeeAsync(employee));

            if (insertResult.Success)
            {
                WindowResult.Action = UserAction.Add;
                WindowResult.Entity = insertResult.Entity.Adapt<EmployeeDisplayModel>();

                var windowArgs = new WindowArgs
                {
                    InfoViewSettings = new InfoViewSettings
                    {
                        Text = ResourceManager.GetString("EmployeeCreatedText"),
                        ModalWindowType = ModalWindowType.Success,
                        RejectButtonVisibility = Visibility.Collapsed,
                        AcceptButtonContent = ResourceManager.GetString("OkButton"),
                        ModalWindowTitle = ResourceManager.GetString("InfoModalName")
                    }
                };

                OnCloseWindow(windowArgs);
            }
        }

        #endregion
    }
}
