﻿using Prism.Commands;

namespace Ribbon.Commands
{
    public class AppCompositeCommands
    {
        public static CompositeCommand UpdateEmployeeCommand { get; set; }

        public static CompositeCommand CreateEmployeeCommand { get; set; }

        public static CompositeCommand DeleteEmployeeCommand { get; set; }

        public static CompositeCommand RefreshCityDataCommand { get; set; }

        public static CompositeCommand CreateNewPositionCommand { get; set; }

        public static CompositeCommand OpenSettingsCommand { get; set; }

        public static CompositeCommand SelectCityCommand { get; set; }

        public static CompositeCommand CreateNewCityCommand { get; set; }

        public static CompositeCommand DuplicateEmployeeCommand { get; set; }

        public static CompositeCommand SortEmployeesCommand { get; set; }

        public static CompositeCommand ClearFilterCommand { get; set; }

        public static CompositeCommand ExportToExcelCommand { get; set; }

        public static CompositeCommand ExportToCsvCommand { get; set; }

        static AppCompositeCommands()
        {
            UpdateEmployeeCommand = new CompositeCommand();
            CreateEmployeeCommand = new CompositeCommand();
            DeleteEmployeeCommand = new CompositeCommand();
            RefreshCityDataCommand = new CompositeCommand();
            CreateNewPositionCommand = new CompositeCommand();
            OpenSettingsCommand = new CompositeCommand();
            SelectCityCommand = new CompositeCommand();
            CreateNewCityCommand = new CompositeCommand();
            DuplicateEmployeeCommand = new CompositeCommand();
            SortEmployeesCommand = new CompositeCommand();
            ClearFilterCommand = new CompositeCommand();
            ExportToExcelCommand = new CompositeCommand();
            ExportToCsvCommand = new CompositeCommand();
        }
    }
}
