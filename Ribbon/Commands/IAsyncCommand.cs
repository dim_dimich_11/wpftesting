﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace Ribbon.Commands
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter = null);
    }
}
