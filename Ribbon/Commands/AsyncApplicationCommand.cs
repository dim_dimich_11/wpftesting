﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ribbon.Commands
{
    public class AsyncApplicationCommand : IAsyncCommand
    {
        private readonly Func<object, Task> _executeAsync;
        private readonly Func<object, bool> _canExecute;

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }

        }

        public AsyncApplicationCommand(Func<object, Task> execute, Func<object, bool> canExecute = null)
        {
            _executeAsync = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute == null || _canExecute(parameter);
        }

        public async void Execute(object parameter = null)
        {
            await ExecuteAsync(parameter);
        }

        public async Task ExecuteAsync(object parameter = null)
        {
            await _executeAsync(parameter);
        }
    }
}
